import math
import random
import numpy as np
import matplotlib.pyplot as plt

print("Hola, para empezar introduzca los parámetros siguientes:")
print("¿Cuántas neuronas quiere poner en la primera capa oculta de la red neuronal?")
numNeuronHiddenLayer1 = input('Yo: ')
numNeuronHiddenLayer1 = int(numNeuronHiddenLayer1)

print("¿Cuántas neuronas quiere poner en la segunda capa oculta de la red neuronal?")
numNeuronHiddenLayer2 = input('Yo: ')
numNeuronHiddenLayer2 = int(numNeuronHiddenLayer2)

print("¿Cuántos elementos quiere poner en cada una de las generaciones?")
numGenerationItems = input('Yo: ')  # Population
numGenerationItems = int(numGenerationItems)

print("¿Cuantas generaciones quiere iterar antes de tener la solución final?")
numGeneration = input('Yo: ')
numGeneration = int(numGeneration)


# Constants
neuralNetworkInputSize = 3
neuralNetworkOutputSize = 1
# Index of the input to be used for testing the network and not for training
testingIndex = 5

# crossover
crossoverRange = neuralNetworkInputSize * numNeuronHiddenLayer1 + numNeuronHiddenLayer1 * numNeuronHiddenLayer2 + numNeuronHiddenLayer2 * neuralNetworkOutputSize + numNeuronHiddenLayer1 + numNeuronHiddenLayer2 + neuralNetworkOutputSize

# population that survives every generation
survivalPercentage = 0.1


# Utils
def printMatrices(matrices):
    for matrix in matrices:
        print(matrix)


# Logic function to estimate with Neural Network
def logicFunction(a, b, c):
    return (c & a) | b


totalLogicInputs = (
    [0, 0, 0],
    [0, 0, 1],
    [0, 1, 0],
    [0, 1, 1],
    [1, 0, 0],
    [1, 0, 1],  # -> not used for training by default
    [1, 1, 0],
    [1, 1, 1],
)

# Apply logic function to all possible inputs
totalLogicOutputs = []
for logicInput in totalLogicInputs:
    totalLogicOutputs.append(logicFunction(logicInput[0], logicInput[1], logicInput[2]))

trainingLogicInputs = totalLogicInputs[0:testingIndex] + totalLogicInputs[testingIndex + 1:]
testingLogicInput = totalLogicInputs[testingIndex]
trainingLogicOutputs = totalLogicOutputs[0:testingIndex] + totalLogicOutputs[testingIndex + 1:]
testingLogicOutput = totalLogicOutputs[testingIndex]


def generateRandomWeights(inputSize, firstLayerSize, secondLayerSize, outputSize):
    return (
        (np.random.rand(firstLayerSize, inputSize) - 0.5) * 4,
        (np.random.rand(secondLayerSize, firstLayerSize) - 0.5) * 4,
        (np.random.rand(outputSize, secondLayerSize) - 0.5) * 4
    )


def generateRandomBiases(firstLayerSize, secondLayerSize, outputSize):
    return (
        (np.random.rand(firstLayerSize) - 0.5) / 2,
        (np.random.rand(secondLayerSize) - 0.5) / 2,
        (np.random.rand(outputSize) - 0.5) / 2
    )


def generateRandomWeightAndBiases(inputSize, firstLayerSize, secondLayerSize, outputSize):
    return [
        generateRandomWeights(inputSize, firstLayerSize, secondLayerSize, outputSize),
        generateRandomBiases(firstLayerSize, secondLayerSize, outputSize)
    ]


# Normalize to 0-1
def activationFunction(val):
    if val > 1:
        return 1
    elif val < -1:
        return -1
    else:
        return val


# Dirac
def outputActivationFunction(val):
    if val > 0:
        return 1
    else:
        return -1


def calculateSignal(inputValues, weights, biases, activationFunc):
    normalizationFactor = len(inputValues)
    matricesProduct = np.dot(weights, inputValues)
    normalizedOutput = matricesProduct / normalizationFactor
    biasedSignal = normalizedOutput + biases
    activatedSignal = activationFunc(biasedSignal)
    return activatedSignal


def calculateNeuralNetwork(inputValues, weightsAndBiases):
    weights = weightsAndBiases[0]
    biases = weightsAndBiases[1]
    vectorizedActivationFunction = np.vectorize(activationFunction)
    firstLayerValues = calculateSignal(inputValues, weights[0], biases[0], vectorizedActivationFunction)
    secondLayerValues = calculateSignal(firstLayerValues, weights[1], biases[1], vectorizedActivationFunction)
    return calculateSignal(secondLayerValues, weights[2], biases[2], outputActivationFunction)


def testNeuralNetwork(inputs, weightsAndBiases):
    outputValues = []
    for i in inputs:
        normalizedOutputValues = (calculateNeuralNetwork(i, weightsAndBiases) + 1) / 2
        outputValues.append(normalizedOutputValues)
    return np.array(outputValues)


def fitnessFunction(expectedOutputs, actualOutputs):
    return (np.array(expectedOutputs) == actualOutputs).sum()


def generatePopulation(popSize):
    res = []
    for i in range(popSize):
        res.append(
            generateRandomWeightAndBiases(neuralNetworkInputSize, numNeuronHiddenLayer1, numNeuronHiddenLayer2,
                                          neuralNetworkOutputSize))
    return res


def testPopulation(populationToBeTested):
    res = []
    for i in populationToBeTested:
        output = testNeuralNetwork(trainingLogicInputs, i)
        score = fitnessFunction(trainingLogicOutputs, output)
        res.append([score, output, i])
    return res


def calculateGeneration(population):
    testedPopulation = testPopulation(population)
    # Sort tested population by score
    testedPopulation.sort(key=lambda x: x[0], reverse=True)
    return testedPopulation


def getStatistics(population):
    average = 0
    peak = 0
    for p in population:
        average = average + p[0]
        if peak < p[0]:
            peak = p[0]
    average = average / len(population)
    return average, peak


def removeResults(survivors):
    res = []
    for i in survivors:
        res.append(i[2])
    return res


# To reproduce from parents two random parents are selected and part of their values are used to generate a new child
# A random point is chosen to change from which parent the vales are from
def reproducePopulation(survivors, populationToBeReproduced):
    for person in range(populationToBeReproduced):
        crossoverPoint = random.randint(0, crossoverRange - 1)
        newPerson = generateRandomWeightAndBiases(neuralNetworkInputSize, numNeuronHiddenLayer1, numNeuronHiddenLayer2,
                                                  neuralNetworkOutputSize)
        firstParent = survivors[random.randint(0, len(survivors) - 1)]
        secondParent = survivors[random.randint(0, len(survivors) - 1)]
        i = 0
        # weights
        # layers of neural network
        for j in range(len(firstParent[0])):
            # rows layer
            for k in range(len(firstParent[0][j])):
                # for columns in layer
                for m in range(firstParent[0][j][0].size):
                    if i < crossoverPoint:
                        newPerson[0][j][k][m] = firstParent[0][j][k][m]
                    else:
                        newPerson[0][j][k][m] = secondParent[0][j][k][m]
                    i = i + 1
        # biases
        # layers of neural network
        for j in range(len(firstParent[0])):
            # rows layer
            for k in range(len(firstParent[0][j])):
                if i < crossoverPoint:
                    newPerson[1][j][k] = firstParent[1][j][k]
                else:
                    newPerson[1][j][k] = secondParent[1][j][k]
                i = i + 1
        survivors.append(newPerson)
    return survivors


def naturalSelection(population):
    survivorsSize = math.ceil(numGenerationItems * survivalPercentage)
    survivors = population[:survivorsSize]
    populationToBeReproduced = numGenerationItems - survivorsSize
    return reproducePopulation(survivors, populationToBeReproduced)


def calculateGenerations(initialPopulation):
    results = []
    for generation in range(numGeneration):
        population = calculateGeneration(initialPopulation)
        [averageFitness, peakFitness] = getStatistics(population)
        results.append([averageFitness, peakFitness, population])
        population = removeResults(population)
        initialPopulation = naturalSelection(population)
    return results


def getResultDataPoints(results):
    averageValues = []
    peakValues = []
    for res in results:
        averageValues.append(res[0])
        peakValues.append(res[1])
    return averageValues, peakValues


starterPopulation = generatePopulation(numGenerationItems)
result = calculateGenerations(starterPopulation)

# Check if best case can solve untrained input
bestNeuralNetworkValues = result[numGeneration - 1][2][0][2]
testingOutput = testNeuralNetwork([testingLogicInput], bestNeuralNetworkValues)
if testingOutput == testingLogicOutput:
    print('Test value predicted correctly')
else:
    print('Test vale was not predicted correctly')

# Plot result
[averages, peaks] = getResultDataPoints(result)
plt.plot(averages)
plt.plot(peaks)
plt.show()
