import numpy
import pygad
import pygad.nn
import pygad.gann

"""ENUNCIADO PRÁCTICA PYTHON/NN/GA
El software desarrollado con python debe hacer:

1. Pedir información al usuario:

Pedir al usuario cuantas neuronas quiere poner en la primera capa oculta de la red neuronal
Pedir al usuario cuantas neuronas quiere poner en la segunda capa oculta de la red neuronal
Preguntar al usuario cuantos elementos quiere poner en cada una de las generaciones
Preguntar al usuario cuantas generaciones quiere iterar antes de tener la solución final

2. Se escogerá una ecuación lógica simple que conste de 3 entradas.

3. Se calcularan todas las posibles salidas para estas entradas.

4. Para entrenar la red neuronal se usarán 7 de las 8 combinaciones posibles con sus 7 resultados.

5. Una vez acabada de entrenar la red. Se mostrará por pantalla un gráfico de cómo ha evolucionado el fitness (o el error para ver cómo baja) de la mejor red de cada una de las generaciones.

6. Se cogerá la mejor red de la última generación, y se le pasará el valor que falta del entrenamiento para ver si se resuelve bien.

Se realizará a través del repositorio de código que se usa normalmente."""

print("Hola, para empezar introduzca los parámetros siguientes:")
print("¿Cuántas neuronas quiere poner en la primera capa oculta de la red neuronal?")

numNeuronHiddenLayer1 = input('Yo: ')
numNeuronHiddenLayer1 = int(numNeuronHiddenLayer1)

print("¿Cuántas neuronas quiere poner en la segunda capa oculta de la red neuronal?")

numNeuronHiddenLayer2 = input('Yo: ')
numNeuronHiddenLayer2 = int(numNeuronHiddenLayer2)

print("¿Cuántos elementos quiere poner en cada una de las generaciones?")

numGenerationItems = input('Yo: ')  # Population
numGenerationItems = int(numGenerationItems)

print("¿Cuantas generaciones quiere iterar antes de tener la solución final?")

numGeneration = input('Yo: ')
numGeneration = int(numGeneration)

# Population that survives
survivals = int(round(numGenerationItems * 10 / 100))
if survivals == 0:
    survivals = 1


def logicFunction(a, b, c):
    return (c & a) | b


"""Our logic function is:  (c AND a) OR b
   The truth table is: 
    000 -> 0
    001 -> 0
    010 -> 1
    011 -> 1
    100 -> 0
    101 -> 1
    110 -> 1
    111 -> 1
"""


def fitness_func(solution, sol_idx):
    global GANN_instance, data_inputs, data_outputs

    predictions = pygad.nn.predict(last_layer=GANN_instance.population_networks[sol_idx],
                                   data_inputs=data_inputs)
    correct_predictions = numpy.where(predictions == data_outputs)[0].size
    solution_fitness = (correct_predictions / data_outputs.size) * 100

    return solution_fitness


def callback_generation(ga_instance):
    global GANN_instance

    population_matrices = pygad.gann.population_as_matrices(population_networks=GANN_instance.population_networks,
                                                            population_vectors=ga_instance.population)

    GANN_instance.update_population_trained_weights(population_trained_weights=population_matrices)

    # print("Generation = {generation}".format(generation=ga_instance.generations_completed))
    # print("Accuracy   = {fitness}".format(fitness=ga_instance.best_solution()[1]))


# Truth table
data_inputs = numpy.array([[0, 0, 0],  # Input
                           [0, 0, 1],
                           [0, 1, 0],
                           # [0, 1, 1],
                           [1, 0, 0],
                           [1, 0, 1],
                           [1, 1, 0],
                           [1, 1, 1]])

data_outputs = numpy.array([0,  # Output
                            0,
                            1,
                            # 1,
                            0,
                            1,
                            1,
                            1])

GANN_instance = pygad.gann.GANN(num_solutions=numGenerationItems,  # Population
                                num_neurons_input=3,  # Input number in this case is 3
                                # Hidden Layers and their neurons which are received by console.
                                num_neurons_hidden_layers=[numNeuronHiddenLayer1, numNeuronHiddenLayer2],
                                num_neurons_output=2,
                                # Type of activation function ReLU (Rectified Linear Unit)
                                hidden_activations=["relu", "relu"],
                                output_activation="softmax")

population_vectors = pygad.gann.population_as_vectors(population_networks=GANN_instance.population_networks)

ga_instance = pygad.GA(num_generations=numGeneration,  # Number of Generations
                       num_parents_mating=survivals,  # Population that survives, 10% of the population
                       initial_population=population_vectors.copy(),
                       fitness_func=fitness_func,
                       mutation_percent_genes=10,
                       callback_generation=callback_generation)

ga_instance.run()
ga_instance.plot_result()

solution, solution_fitness, solution_idx = ga_instance.best_solution()
print('fitness')
print(solution_fitness)

predictions = pygad.nn.predict(last_layer=GANN_instance.population_networks[solution_idx],
                               data_inputs=numpy.array([[0, 1, 1]]))

print('Expected prediction to be 1')
print("Predictions of the trained network : {predictions}".format(predictions=predictions))
